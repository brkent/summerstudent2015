#! /usr/bin/env python
#
# Simple python script for displaying a 2D polar image representation of a Bessel Function
# Brian R. Kent
# National Radio Astronomy Observatory
# Last update June 30, 2015
#
import pylab
import string
import math
import os
import sys
import time
import numpy
import scipy
import scipy.special


def main():
	eth = 2
	er = 2
	wc = 1
	m = 4

	r=numpy.linspace(0,10,100)
	phi=numpy.linspace(0,2*numpy.pi,360)
	z=scipy.zeros((len(r),len(phi)), float)  


	for i in range(0,len(r)):
		for j in range(0,len(phi)):
			z[i,j]=numpy.real(scipy.special.jn((m*numpy.sqrt(eth))/numpy.sqrt(er),numpy.sqrt(eth)*r[i]*wc)*numpy.exp(1j*phi[j]*m))

	pylab.figure()
	ax = pylab.subplot(111, projection='polar')	 
	cs = ax.pcolormesh(phi, r, z, cmap=pylab.cm.jet)	
	cbar = pylab.colorbar(cs)
	pylab.suptitle('Bessel Function Polar Projection')
	pylab.xlabel("Angle (deg)")
	ax.axes.get_yaxis().set_visible(True)
	ax.grid(True)
	pylab.show()

#-----------------------------------------------------------------
#Begin here
if __name__ == "__main__":
    main()
